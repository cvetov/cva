module com.example.chocolaterestoran {
    requires javafx.controls;
    requires javafx.fxml;
    requires java.sql;
    requires itextpdf;


    opens com.example.sportshop to javafx.fxml;
    exports com.example.sportshop;
}
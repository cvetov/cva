package com.example.sportshop;

import java.sql.SQLException;

public class Connect {
    protected ConnectDB db;

    protected int counterId;

    Connect() throws SQLException {
        db = new ConnectDB();
        initTable();
    }


    void initTable() {
        try {
            db.execute("CREATE TABLE IF NOT EXISTS doljnost (\n" +
                    "  id_doljnost INT(11) NOT NULL,\n" +
                    "  name_doljnost TEXT NOT NULL,\n" +
                    "  PRIMARY KEY (id_doljnost))\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");
            db.execute("CREATE TABLE IF NOT EXISTS tipmat (\n" +
                    "  idtipmat INT(11) NOT NULL,\n" +
                    "  tipmat VARCHAR(45) NOT NULL,\n" +
                    "  PRIMARY KEY (idtipmat))\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");


            db.execute("CREATE TABLE IF NOT EXISTS tipvh (\n" +
                    "  idtipvh INT(11) NOT NULL,\n" +
                    "  tipvh VARCHAR(45) NOT NULL,\n" +
                    "  PRIMARY KEY (idtipvh))\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");


            db.execute("CREATE TABLE IF NOT EXISTS sotrudnik (\n" +
                    "  sotrudnic_cod INT(11) NOT NULL,\n" +
                    "  fio TEXT NOT NULL,\n" +
                    "  login TEXT NOT NULL,\n" +
                    "  pasword TEXT NOT NULL,\n" +
                    "  posled_vhod DATETIME NOT NULL,\n" +
                    "  doljnost_id_doljnost INT(11) NOT NULL,\n" +
                    "  tipvh_idtipvh INT(11) NOT NULL,\n" +
                    "  PRIMARY KEY (sotrudnic_cod),\n" +
                    "  CONSTRAINT fk_sotrudnik_doljnost\n" +
                    "    FOREIGN KEY (doljnost_id_doljnost)\n" +
                    "    REFERENCES doljnost (id_doljnost)\n" +
                    "    ON DELETE NO ACTION\n" +
                    "    ON UPDATE NO ACTION,\n" +
                    "  CONSTRAINT fk_sotrudnik_tipvh1\n" +
                    "    FOREIGN KEY (tipvh_idtipvh)\n" +
                    "    REFERENCES tipvh (idtipvh)\n" +
                    "    ON DELETE NO ACTION\n" +
                    "    ON UPDATE NO ACTION)\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");

            db.execute("CREATE TABLE IF NOT EXISTS materials (\n" +
                    "  id INT(11) NOT NULL,\n" +
                    "  name_mater TEXT NOT NULL,\n" +
                    "  izobrag TEXT NOT NULL,\n" +
                    "  opisan TEXT NOT NULL,\n" +
                    "  cena INT(11) NOT NULL,\n" +
                    "  col_sclad INT(11) NOT NULL,\n" +
                    "  Mincol INT(11) NOT NULL,\n" +
                    "  Kolypac INT(11) NOT NULL,\n" +
                    "  ed_izmer TEXT NOT NULL,\n" +
                    "  tipmat_idtipmat INT(11) NOT NULL,\n" +
                    "  PRIMARY KEY (id),\n" +
                    "  CONSTRAINT fk_materials_tipmat1\n" +
                    "    FOREIGN KEY (tipmat_idtipmat)\n" +
                    "    REFERENCES tipmat (idtipmat)\n" +
                    "    ON DELETE NO ACTION\n" +
                    "    ON UPDATE NO ACTION)\n" +
                    "ENGINE = InnoDB\n" +
                    "DEFAULT CHARACTER SET = utf8;");


            db.execute("INSERT IGNORE INTO  doljnost (id_doljnost,name_doljnost) VALUES (1,'продавец');\n" +
                    "INSERT IGNORE INTO doljnost  (id_doljnost,name_doljnost) VALUES (2,'администратор');\n" +
                    "INSERT IGNORE INTO  doljnost (id_doljnost,name_doljnost) VALUES (3,'старший смены');");
            db.execute("INSERT IGNORE INTO tipmat  (idtipmat,tipmat) VALUES (1,'Гранулы');\n" +
                    "INSERT IGNORE INTO tipmat  (idtipmat,tipmat) VALUES (2,'Краски');\n" +
                    "INSERT IGNORE INTO tipmat  (idtipmat,tipmat) VALUES (3,'Нитки');\n");
            db.execute("INSERT IGNORE INTO tipvh  (idtipvh,tipvh) VALUES (1,'Успешно');\n" +
                    "INSERT IGNORE INTO tipvh  (idtipvh,tipvh) VALUES (2,'Не успешно');");
            db.execute("INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (1,'Гранулы белый 2x2','нет',' ',47680,76,8,7,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (2,'Нить серый 1x0','нет',' ',27456,978,42,1,'м',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (3,'Нить белый 1x3','нет',' ',2191,406,27,8,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (4,'Нить цветной 1x1','\\\\materials\\\\image_5.jpeg',' ',8619,424,10,3,'г',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (5,'Нить цветной 2x0','нет',' ',16856,395,26,2,'м',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (6,'Краска синий 2x2','нет',' ',403,334,48,6,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (7,'Нить синий 0x2','нет',' ',7490,654,10,9,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (8,'Гранулы серый 2x2','нет',' ',15478,648,17,7,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (9,'Краска синий 1x2','нет',' ',44490,640,50,2,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (10,'Нить зеленый 2x0','\\\\materials\\\\image_10.jpeg',' ',28301,535,45,5,'м',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (11,'Гранулы синий 1x2','нет',' ',9242,680,6,3,'кг',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (12,'Нить синий 3x2','нет',' ',10878,529,13,1,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (13,'Краска белый 2x2','\\\\materials\\\\image_3.jpeg',' ',29906,659,35,1,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (14,'Краска зеленый 0x3','нет',' ',24073,50,48,2,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (15,'Нить зеленый 2x3','нет',' ',20057,649,25,7,'г',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (16,'Краска белый 2x1','нет',' ',3353,790,8,2,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (17,'Нить серый 2x3','нет',' ',22452,431,40,1,'г',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (18,'Гранулы серый 3x2','нет',' ',29943,96,9,5,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (19,'Краска серый 3x2','нет',' ',55064,806,50,3,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (20,'Гранулы белый 0x3','нет',' ',7183,538,11,3,'кг',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (21,'Краска цветной 1x1','нет',' ',43466,784,22,3,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (22,'Гранулы белый 1x0','нет',' ',27718,980,41,3,'кг',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (23,'Краска серый 0x2','нет',' ',33227,679,36,3,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (24,'Гранулы серый 3x3','нет',' ',15170,2,38,5,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (25,'Краска серый 3x0','нет',' ',19352,341,38,7,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (26,'Гранулы синий 2x1','\\\\materials\\\\image_2.jpeg',' ',231,273,17,9,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (27,'Гранулы синий 0x2','нет',' ',41646,576,36,9,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (28,'Нить цветной 1x0','нет',' ',24948,91,38,5,'г',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (29,'Краска зеленый 2x2','нет',' ',19014,752,36,2,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (30,'Краска цветной 1x3','нет',' ',268,730,5,9,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (31,'Краска серый 2x0','нет',' ',35256,131,22,2,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (32,'Нить зеленый 2x1','нет',' ',34556,802,16,6,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (33,'Краска цветной 0x3','нет',' ',3322,324,9,10,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (34,'Нить белый 2x3','нет',' ',10823,283,41,3,'г',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (35,'Гранулы синий 3x0','нет',' ',16665,411,8,1,'кг',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (36,'Гранулы синий 1x3','нет',' ',5668,41,30,8,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (37,'Нить цветной 2x1','нет',' ',7615,150,22,3,'м',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (38,'Гранулы серый 3x0','\\\\materials\\\\image_7.jpeg',' ',702,0,5,4,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (39,'Краска синий 3x0','нет',' ',38644,523,42,7,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (40,'Нить зеленый 0x0','нет',' ',41827,288,43,8,'м',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (41,'Гранулы белый 1x2','нет',' ',8129,77,46,4,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (42,'Краска белый 3x0','нет',' ',51471,609,48,5,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (43,'Краска цветной 0x1','нет',' ',54401,43,8,6,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (44,'Нить серый 1x1','нет',' ',14474,372,22,5,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (45,'Краска синий 2x1','нет',' ',46848,642,29,9,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (46,'Нить серый 3x0','нет',' ',29503,409,19,1,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (47,'Краска зеленый 3x3','нет',' ',27710,601,32,6,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (48,'Краска синий 2x0','нет',' ',40074,135,50,7,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (49,'Гранулы синий 2x3','нет',' ',53482,749,45,2,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (50,'Нить синий 0x3','нет',' ',32087,615,22,8,'м',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (51,'Нить синий 3x3','нет',' ',45774,140,12,7,'г',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (52,'Краска зеленый 2x3','нет',' ',44978,485,8,2,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (53,'Нить синий 3x0','нет',' ',44407,67,23,10,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (54,'Гранулы серый 2x1','нет',' ',50339,779,44,7,'кг',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (55,'Краска зеленый 0x1','нет',' ',30581,869,7,2,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (56,'Краска синий 0x0','нет',' ',18656,796,29,8,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (57,'Краска серый 2x1','нет',' ',46579,706,45,5,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (58,'Нить белый 0x1','нет',' ',36883,101,43,10,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (59,'Гранулы зеленый 1x2','нет',' ',45083,575,15,9,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (60,'Краска серый 0x1','нет',' ',35063,768,27,2,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (61,'Гранулы цветной 0x1','нет',' ',24488,746,50,3,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (62,'Гранулы белый 3x1','нет',' ',43711,995,27,8,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (63,'Нить зеленый 0x2','нет',' ',17429,578,20,2,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (64,'Гранулы зеленый 0x2','нет',' ',38217,206,34,4,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (65,'Краска цветной 1x2','нет',' ',47701,299,50,10,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (66,'Краска зеленый 1x0','нет',' ',52189,626,17,8,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (67,'Гранулы серый 0x0','нет',' ',16715,608,12,5,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (68,'Гранулы синий 0x3','нет',' ',45134,953,48,5,'кг',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (69,'Краска цветной 2x1','нет',' ',1846,325,45,1,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (70,'Нить синий 2x3','нет',' ',43659,10,21,5,'м',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (71,'Нить синий 2x1','нет',' ',12283,948,13,9,'г',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (72,'Гранулы белый 1x1','нет',' ',6557,93,11,4,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (73,'Краска синий 1x3','нет',' ',38230,265,17,6,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (74,'Краска зеленый 3x0','\\\\materials\\\\image_1.jpeg',' ',20226,261,7,2,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (75,'Нить зеленый 1x0','нет',' ',8105,304,43,9,'г',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (76,'Краска цветной 0x2','нет',' ',2600,595,38,7,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (77,'Нить синий 3x1','нет',' ',4920,579,48,7,'м',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (78,'Краска зеленый 0x2','нет',' ',39809,139,23,9,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (79,'Краска синий 3x3','нет',' ',46545,740,24,6,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (80,'Краска зеленый 1x1','\\\\materials\\\\image_6.jpeg',' ',40583,103,34,2,'кг',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (81,'Краска цветной 2x3','нет',' ',46502,443,46,9,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (82,'Нить цветной 3x0','нет',' ',53651,989,14,1,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (83,'Гранулы серый 2x3','нет',' ',47757,467,28,6,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (84,'Краска белый 1x0','нет',' ',3543,95,6,6,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (85,'Гранулы серый 3x1','нет',' ',10899,762,6,10,'кг',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (86,'Гранулы серый 2x0','\\\\materials\\\\image_8.jpeg',' ',8939,312,21,3,'кг',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (87,'Нить белый 0x2','нет',' ',29271,43,19,4,'г',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (88,'Гранулы зеленый 1x1','\\\\materials\\\\image_4.jpeg',' ',46455,10,19,4,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (89,'Нить серый 0x2','\\\\materials\\\\image_9.jpeg',' ',45744,504,10,3,'м',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (90,'Гранулы белый 0x2','нет',' ',9330,581,40,2,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (91,'Нить цветной 3x2','нет',' ',2939,831,46,3,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (92,'Гранулы белый 3x0','нет',' ',50217,208,7,6,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (93,'Нить серый 1x2','нет',' ',30198,292,30,1,'м',3);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (94,'Краска белый 0x1','нет',' ',19777,423,47,7,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (95,'Гранулы цветной 0x3','нет',' ',1209,723,44,7,'кг',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (96,'Нить серый 1x3','нет',' ',32002,489,25,1,'г',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (97,'Гранулы белый 2x3','нет',' ',32446,274,8,4,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (98,'Краска зеленый 3x1','нет',' ',32487,657,19,10,'л',2);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (99,'Гранулы цветной 3x2','нет',' ',28596,32,45,1,'л',1);\n" +
                    "INSERT IGNORE INTO materials  (id,name_mater,izobrag,opisan,cena,col_sclad,Mincol,Kolypac,ed_izmer,tipmat_idtipmat) VALUES (100,'Нить белый 2x0','нет',' ',46684,623,23,2,'м',2);\n");

            db.execute("INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (101,'Иванов Иван Иванович','1','1','2022-05-15 13:13:00',1,1);\n" +
                    "INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (102,'Петров Петр Петрович','fdfs','uzWC67','2022-05-15 13:13:00',1,1);\n" +
                    "INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (103,'Федоров Федор Федорович','2','2','2022-05-15 13:13:00',2,1);\n" +
                    "INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (104,'Миронов Вениамин Куприянович','3','3','2022-05-15 13:13:00',3,1);\n" +
                    "INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (105,'Ширяев Ермолай Вениаминович','shiryev@namecomp.ru','RSbvHv','2022-05-15 13:13:00',3,2);\n" +
                    "INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (106,'Игнатов Кассиан Васильевич','ignatov@namecomp.ru','rwVDh9','2022-05-15 13:13:00',3,1);\n" +
                    "INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (107,'Хохлов Владимир Мэлсович','hohlov@namecomp.ru','LdNyos','2022-05-15 13:13:00',1,1);\n" +
                    "INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (108,'Стрелков Мстислав Георгьевич','strelkov@namecomp.ru','gynQMT','2022-05-15 13:13:00',1,2);\n" +
                    "INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (109,'Беляева Лилия Наумовна','belyeva@@namecomp.ru','AtnDjr','2022-05-15 13:13:00',1,1);\n" +
                    "INSERT IGNORE INTO sotrudnik  (sotrudnic_cod,fio,login,pasword,posled_vhod,doljnost_id_doljnost,tipvh_idtipvh) VALUES (110,'Смирнова Ульяна Гордеевна','smirnova@@namecomp.ru','JlFRCZ','2022-05-15 13:13:00',1,1);\n");




        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}

package com.example.sportshop;

import java.sql.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;


//Класс для взаимодействия с БД
public class ConnectDB {

    Connection dbConnect;

    //IP адресс сервера
    Map<String, String> env = System.getenv();
    private final String host = env.getOrDefault("DB_HOST", "localhost");

    //Порт
    private final String port =env.getOrDefault("DB_PORT", "3306");

    //Название схемы БД
    private  final String nameDB = env.getOrDefault("DB_NAME", "cva");

    //Имя пользователя в БД
    private  final String userDB = env.getOrDefault("DB_USER", "root");

    //Пароль пользователя от БД
    private  final String passDB = env.getOrDefault("DB_PASS", "");

    //Метод подключения к базе
    public Connection getDbConnection() throws ClassNotFoundException, SQLException{
        String connectionString = "jdbc:mysql://" + host + ":"
                + port + "/" + nameDB;
        Class.forName("com.mysql.cj.jdbc.Driver");

        System.out.println(connectionString);

        dbConnect = DriverManager.getConnection(connectionString,
                userDB , passDB);
        return dbConnect;
    }

    //Метод Авторизации(проверка наличия логина и пароля в БД)
    public ResultSet getUser(String login, String password){
        ResultSet resSet = null;

        //Формирование запроса
        String select = "SELECT doljnost_id_doljnost, fio FROM "+nameDB+".sotrudnik WHERE login = ? and pasword = ?";

        try {
            PreparedStatement prSt = getDbConnection().prepareStatement(select);
            prSt.setString(1, login);
            prSt.setString(2, password);
            resSet = prSt.executeQuery();

        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return resSet;
    }

    public ResultSet getProduct(String select){
        ResultSet resSet = null;

        try {
            PreparedStatement prSt = getDbConnection().prepareStatement(select);
            resSet = prSt.executeQuery();

        } catch (SQLException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return resSet;

    }






    boolean execute(String query) throws SQLException, ClassNotFoundException {
        Statement statement = getDbConnection().createStatement();
        return statement.execute(query);
    }



}

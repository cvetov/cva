package com.example.sportshop;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Admin {

    ConnectDB dbHandler = new ConnectDB();

    @FXML
    private TextField searchField;

    @FXML
    private GridPane gridProduct;

    @FXML
    private Label fioLabel;

    @FXML
    private Button btnOrder;

    @FXML
    private Button btnBack;


    //private List<Product> products = new ArrayList<>();
    int row = 1;

    @FXML
    void initialize() throws SQLException {
        fioLabel.setText(startScreenController.NameFooter);

        //createProduct("SELECT name,price, discription, photo FROM mydb.product");
    }

    public void goCreateOrder() throws IOException {
        Parent wLogin = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("createOrder.fxml")));
        Stage window = (Stage) btnOrder.getScene().getWindow();
        window.setScene(new Scene(wLogin, 1000, 540));
        window.show();

    }


    public void btnbac() throws IOException {
        Parent wLogin = FXMLLoader.load(Objects.requireNonNull(getClass().getResource("startScreen.fxml")));
        Stage window = (Stage) btnBack.getScene().getWindow();
        window.setScene(new Scene(wLogin, 960, 540));
        window.show();
    }


}

# desktop-приложениt для продажи компьютерной техники
Desctop-приложение с графическим интерфейсом и связью с БД
# Зависимости
Java 19 SDK
# Сборка проекта
<pre>
$ bash mvnw install
$ bash mvnw dependency:copy-dependencies
</pre>
# Запуск
После сборки
<pre>
$ DB_HOST=localhost \
DB_PORT=3306 \
DB_NAME=demo_up04 \
DB_USER=root \
DB_PASS=root \
java --module-path=target/dependency/ --add-modules javafx.controls,javafx.fxml -cp target/demo_up04-1.0-SNAPSHOT.jar com.example.demo_up04.DemoApplication
</pre>
